class Model:
    def __init__(self, hparams = {}):
        '''
        General model capable of interacting of the data
        '''
        self.hparams = hparams
        self.proba = None
    def fit(self, ds):
        pass

    def predict(self, ds):
        pass

    def predict_proba(self, ds):
        pass

    def reset(self, proba):
        '''
        Change the frontier at which the model switches from 0 to 1
        '''
        self.proba = proba
