from sklearn.model_selection._search import ParameterGrid
import numpy as np
from .model import Model
from ..datasources import Datasource
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook


from sklearn.model_selection import train_test_split


class ModelSelectionBack(Model):

    def __init__(self, model, objfunction, tqdm = False, grid_size = 100):
        '''
        Performs a backward selection of the Features based on the metrics in objfunction
        ''' 
        self.model = model
        self.function = objfunction
        self.tqdm = tqdm
        if self.tqdm:
            self.tqdm_fun = lambda x : tqdm_notebook(x)
        else:
            self.tqdm_fun = lambda x : x

    def fit(self, ds, split = 0.6):
        '''
        Splits the data into train and dev
        For each subsample of the features, train the model and compute objfunction on dev set
        Remove the worst feature and go on until you cannot remove any features without reducing the objfunction
        '''
        X = ds.getData().loc[:, ds.getFeatures()]
        y = ds.getData().loc[:, ds.getLabel()]
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1 - split)

        #print(pd.concat([X_train, y_train], axis = 1))
        ds_train = Datasource(data = pd.concat([X_train, y_train], axis = 1),
                                   features = ds.getFeatures(),
                                   label = ds.getLabel())
        ds_dev = Datasource(data = pd.concat([X_test, y_test], axis = 1),
                             features = ds.getFeatures(),
                             label = ds.getLabel())
        stay = True
        li_features_to_keep = [x for x in ds.getFeatures()]
        results = []
        best_score = 0
        while stay:
            best_feature_to_remove = None
            for k, feat in self.tqdm_fun(enumerate(ds.getFeatures())):
                li_features = [x for x in li_features_to_keep if x != feat]
                ds_train_test = ds_train.subsample(li_features)
                ds_dev_test = ds_dev.subsample(li_features)
                self.model.fit(ds_train_test)
                pred = self.model.predict_proba(ds_dev_test)

                score = self.function.evaluate(pred > 0.5, ds_dev_test.getData().loc[:, ds_dev_test.getLabel()])
                if score > best_score:
                    best_score = score
                    best_feature_to_remove = feat

            if best_feature_to_remove:
                li_features_to_keep = [x for x in li_features_to_keep if x != best_feature_to_remove]
                results.append((best_score, len(li_features_to_keep)))
            else:
                stay=False

        results.sort()
        ds_train_test = ds_train.subsample(li_features_to_keep)
        self.model.fit(ds_train_test)
        self.results = results
        self.features = li_features_to_keep


    def predict(self, ds):
        '''
        Only keep the features selected in the fit, and predict for the test set
        '''
        ds_pred = ds.subsample(self.features)
        return self.model.predict(ds_pred)


    def plot_score_curve(self):
        '''
        Plot the objfunction compared to the number of selected principal components
        '''
        plt.figure()
        self.results.sort(key = lambda x : x[1])
        plt.plot(np.array([x[1] for x in self.results]), [x[0] for x in self.results])
