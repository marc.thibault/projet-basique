from sklearn.model_selection._search import ParameterGrid
import numpy as np
from .model import Model
from ..datasources import Datasource
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook


from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA

class PCASelection(Model):

    def __init__(self, model, objfunction, tqdm = False):
        '''
        Performs a PCA of the data
        Does a forward selection of the best components to keep based on the metrics in objfunction
        '''
        self.model = model
        self.function = objfunction
        self.tqdm = tqdm
        if self.tqdm:
            self.tqdm_fun = lambda x : tqdm_notebook(x)
        else:
            self.tqdm_fun = lambda x : x

        self.pca = None

    def fit(self, ds, split = 0.6):
        '''
        Splits the data into train and dev
        For each subsample of the principal components, train the model and compute objfunction on dev set
        Keep the best principal component and go on until all subsets are tested
        For robustness, keep as the best model the first model for which we reach 95% of the objfunction of the best
        '''
        X = ds.getData().loc[:, ds.getFeatures()]
        y = ds.getData().loc[:, ds.getLabel()]
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1 - split)

        results = []
        for i in self.tqdm_fun(range(len(ds.getFeatures()))):
            pca = PCA(n_components=i+1)
            pca.fit(X_train)
            X_test_PCA =pca.transform(X_test)
            X_train_PCA = pca.transform(X_train)

            X_train_PCA = pd.DataFrame(index = X_train.index, columns=X_train.columns[0:i+1], data=X_train_PCA)
            X_test_PCA = pd.DataFrame(index = X_test.index, columns=X_test.columns[0:i+1], data=X_test_PCA)

            ds_train = Datasource(data = pd.concat([X_train_PCA, y_train], axis = 1),
                                    features = ds.getFeatures()[:i+1],
                                    label = ds.getLabel())
            ds_dev = Datasource(data = pd.concat([X_test_PCA, y_test], axis = 1),
                                features = ds.getFeatures()[:i+1],
                                label = ds.getLabel())

            self.model.fit(ds_train)
            pred = self.model.predict_proba(ds_dev)

            results.append((self.function.evaluate(pred > 0.5, ds_dev.getData().loc[:, ds_dev.getLabel()]), i+1))

        results.sort()
        max_value = results[-1][0]
        i = 0
        test = results[i][0]
        while test < 0.95 * max_value:
            i += 1
            test = results[i][0]
        pca = PCA(n_components=results[i][1])
        pca.fit(X_train)
        X_test_PCA =pca.transform(X_train)
        X_train_PCA = pca.transform(X_train)
        X_train_PCA = pd.DataFrame(index = X_train.index, columns=X_train.columns[0:results[i][1]], data=X_train_PCA)

        ds_train = Datasource(data = pd.concat([X_train_PCA, y_train], axis = 1),
                                features = ds.getFeatures()[:results[i][1]],
                                label = ds.getLabel())

        self.model.fit(ds_train)
        self.results = results
        self.pca = pca
        self.coeff_PCA = results[i][1]

    def predict(self, ds):
        '''
        Applies the same transformation to the test set, and predicts
        '''
        X_predict = ds.data[ds.getFeatures()].values
        y_train = ds.data[ds.getLabel()]

        X_predict_PCA = self.pca.transform(X_predict)
        X_predict_PCA = pd.DataFrame(index = ds.data.index, columns=ds.getFeatures()[0:self.coeff_PCA], data=X_predict_PCA)

        ds_train = Datasource(data = pd.concat([X_predict_PCA, y_train], axis = 1),
                                features = ds.getFeatures()[:self.coeff_PCA],
                                label = ds.getLabel())
        return self.model.predict(ds_train)


    def plot_score_curve(self):
        '''
        Plot the objfunction compared to the nmber of selected principal components
        '''
        plt.figure()
        self.results.sort(key = lambda x : x[1])
        plt.plot(np.array([x[1] for x in self.results]), [x[0] for x in self.results])
