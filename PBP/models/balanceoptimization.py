from sklearn.model_selection._search import ParameterGrid
import numpy as np
from .model import Model
from ..datasources import Datasource
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook


from sklearn.model_selection import train_test_split


class BalanceOptimization(Model):

    def __init__(self, model, objfunction, tqdm = False, grid_size = 10, grid_min = 0.001, grid_max = 0.999, cv_folds = 5):
        '''
        Resample the dataset to reach a fixed balance between the classes
        '''
        self.model = model
        self.objfunction = objfunction
        self.tqdm = tqdm
        self.grid = np.linspace(grid_min, grid_max, grid_size + 1)
        self.cv_folds = cv_folds
        if self.tqdm:
            self.tqdm_fun = lambda x : tqdm_notebook(x)
        else:
            self.tqdm_fun = lambda x : x


    def fit(self, ds):
        '''
        Create 5 crossval folds, resample each of train set but not the dev, train and compute objfunction on each dev set
        Do it for a grid of weights, and keep the best balance as the good model
        Record the results for each balance
        '''
        self.train_scores_mean = []
        self.train_scores_std = []
        self.dev_scores_mean = []
        self.dev_scores_std = []
        for balance in self.tqdm_fun(self.grid):
            balance_score_train = []
            balance_score_dev = []
            for ds_train, ds_dev in ds.cross_val_folds(self.cv_folds):
                ds_train_balanced = ds_train.balance_data(weight = balance)
                self.model.fit(ds_train_balanced)
                balance_score_train.append(self.objfunction.evaluate(self.model.predict(ds_train), ds_train.getData().loc[:, ds_train.getLabel()]))
                balance_score_dev.append(self.objfunction.evaluate(self.model.predict(ds_dev), ds_dev.getData().loc[:, ds_dev.getLabel()]))
            self.train_scores_mean.append(np.mean(balance_score_train))
            self.train_scores_std.append(np.std(balance_score_train))
            self.dev_scores_mean.append(np.mean(balance_score_dev))
            self.dev_scores_std.append(np.std(balance_score_dev))

        self.optimal_balance = self.grid[np.argmax(self.dev_scores_mean)]
        self.model.fit(ds.balance_data(self.optimal_balance))
        return self


    def predict(self, ds):
        return self.model.predict(ds)

    def plot_score_curves(self):
        '''
        plot the train and dev scores for each balance
        '''
        plt.plot(self.grid, self.train_scores_mean, label = "Train score", c = 'g')
        plt.plot(self.grid, np.array(self.train_scores_mean) + np.array(self.train_scores_std), c = 'g', linestyle = '--')
        plt.plot(self.grid, np.array(self.train_scores_mean) - np.array(self.train_scores_std), c = 'g', linestyle = '--')


        plt.plot(self.grid, self.dev_scores_mean, label = "Dev score", c = 'r')
        plt.plot(self.grid, np.array(self.dev_scores_mean) + np.array(self.dev_scores_std), c = 'r', linestyle = '--')
        plt.plot(self.grid, np.array(self.dev_scores_mean) - np.array(self.dev_scores_std), c = 'r', linestyle = '--')
        plt.legend()
        plt.show()
