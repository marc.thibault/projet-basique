from .model import Model


class SKlearnWrapper(Model):
    def __init__(self, classifier):
        '''
        General wrapper of Sklearn and XGBoost to interact woth our datasources
        ''' 
        super().__init__()
        self.clf = classifier
        self.proba = 0.5

    def fit(self, ds, *args, **kwargs):
        '''
        Call the fit function of the Sklearn model with only a Datasource as argument
        Extract the features and the label, and fits
        '''
        features = ds.getData().loc[:, ds.getFeatures()].values
        label = ds.getData().loc[:, ds.getLabel()].values
        self.clf.fit(features, label, *args, **kwargs)

    def predict(self, ds):
        # features = ds.getData().loc[:, ds.getFeatures()].values
        return self.predict_proba(ds) > self.proba

    def predict_proba(self, ds):
        '''
        Calls predict proba for Sklearn
        The try is intended for XGBoost, which function predict already outputs a proba
        '''
        features = ds.getData().loc[:, ds.getFeatures()].values
        try:
            pred = self.clf.predict_proba(features)[:, 1]
        except:
            pred = self.clf.predict(features)
        return pred
