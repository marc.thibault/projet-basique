'''
Model is the general framework to fit a create, fit, predict and manage a model
SKlearnwrapper is intended to wrap a Sklearn model into our framework so it can interact with a Datasource

All the others are used for a specific exploration of the data
'''
from .frontier_selection import FrontierSelection
from .logistic_regression import LogRegModel
from .sklearn_wrapper import SKlearnWrapper
from .PCA_selection import PCASelection
from .forward_model_selection import ModelSelection
from .backward_model_selection import ModelSelectionBack
from .custom_logistic import CustomLogistic
from .balanceoptimization import BalanceOptimization
