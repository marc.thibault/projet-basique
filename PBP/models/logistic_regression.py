from .model import Model
from sklearn.linear_model import LogisticRegression
from .sklearn_wrapper import SKlearnWrapper

class LogRegModel(SKlearnWrapper):
    def __init__(self, proba=0.5, penalty='l2', C=1.0):
        '''
        Sklearn wrapper around Logistic Regression
        '''
        super().__init__(LogisticRegression(penalty=penalty, C=C))
        self.proba = proba
