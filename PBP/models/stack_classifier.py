from sklearn.model_selection._search import ParameterGrid
import numpy as np
from .model import Model
from ..datasources import Datasource
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook
from itertools import product

from sklearn.model_selection import train_test_split


class StackClassifier(Model):

    def __init__(self, first_layer, model, folds = 5):
        '''
        Applies a two step regression
        Split the data in 'folds' train dev folds
        For each fold
            + Fit each model in first_layer on the train
            + Predict on the dev set
        After this step, we have a prediction for each sample that is not self looking
        Then we fit the model in model
        '''
        self.first_layer = first_layer
        self.model = model
        self.folds = folds
        self.proba = 0.5

    def fit(self, ds):
        '''
        Fit the first layer models, then fit the model taking the output of the first layer as features
        '''
        list_chunks = ds.cross_val_folds(self.folds)
        size = len(list_chunks[0][1].data.values[:, 0])
        prediction = np.zeros((size * self.folds, len(self.first_layer)+1))
        count = 0
        columns = ['predict_' + str(k) for k in range(len(self.first_layer))]
        columns_complete = columns + ['status']
        for ds_train, ds_test in list_chunks:
            for k, model in enumerate(self.first_layer):
                model.fit(ds_train)
                result_1 = model.predict_proba(ds_test)
                prediction[count:count+size, k] = result_1
            prediction[count:count+size, -1] = ds_test.data[ds_test.getLabel()].values
            count += size

        df_results = pd.DataFrame(data=prediction, columns=columns_complete)
        df_results['status'] = df_results['status'].astype(int)
        ds_train_interm = Datasource(data=df_results, label = 'status', features = columns)
        self.model.fit(ds_train_interm)

    def predict_proba(self, ds):
        '''
        Predict each first layer model, then the second layer
        '''
        prediction = np.zeros((ds.data.shape[0],len(self.first_layer)+1))
        columns = ['predict_' + str(k) for k in range(len(self.first_layer))]
        columns_complete = columns + ['status']

        for k, model in enumerate(self.first_layer):
            result_1 = model.predict_proba(ds)

            prediction[:, k] = result_1
        prediction[:, -1] = ds.data[ds.getLabel()].values
        df_results = pd.DataFrame(data=prediction, columns=columns_complete)

        ds_train_interm = Datasource(data=df_results, label = 'status', features = columns)

        return self.model.predict_proba(ds_train_interm)

    def predict(self, ds):
        return self.predict_proba(ds) > self.proba
