from sklearn.model_selection._search import ParameterGrid
import numpy as np
from .model import Model
from ..datasources import Datasource
import pandas as pd
import matplotlib.pyplot as plt
from tqdm import tqdm_notebook


from sklearn.model_selection import train_test_split


class FrontierSelection(Model):

    def __init__(self, model, objfunction, tqdm = False, grid_size = 100):
        '''
        Select the best frontier at which the model switches from predicting 0 to 1
        Efficient sine it only requires one fit and several predicts
        '''
        self.model = model
        self.function = objfunction
        self.tqdm = tqdm
        self.grid_size = grid_size
        if self.tqdm:
            self.tqdm_fun = lambda x : tqdm_notebook(x)
        else:
            self.tqdm_fun = lambda x : x

    def fit(self, ds, split = 0.6):
        '''
        Split the data and fit the model
        Then for a grid of frontiers, predict the dev set with the new frontier and compute objfunction
        Keep the frontier with the best objfunction, and store results
        '''
        grid=np.linspace(0, 1, self.grid_size + 1)
        X = ds.getData().loc[:, ds.getFeatures()]
        y = ds.getData().loc[:, ds.getLabel()]
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1 - split)

        ds_train = Datasource(data = pd.concat([X_train, y_train], axis = 1),
                                   features = ds.getFeatures(),
                                   label = ds.getLabel())
        ds_dev = Datasource(data = pd.concat([X_test, y_test], axis = 1),
                             features = ds.getFeatures(),
                             label = ds.getLabel())


        self.model.fit(ds_train)
        pred = self.model.predict_proba(ds_dev)
        results = []
        for k, param in enumerate(self.tqdm_fun(grid)):
            results.append((self.function.evaluate(pred > param, ds_dev.getData().loc[:, ds_dev.getLabel()]), k))

        results.sort()

        self.results = results
        self.model.reset(grid[results[-1][1]])


    def predict(self, ds):
        return self.model.predict(ds)


    def plot_score_curve(self):
        '''
        Plot objfunction for dev set for each frontier
        '''
        plt.figure()
        self.results.sort(key = lambda x : x[1])
        plt.plot(np.array([x[1] for x in self.results]) / self.grid_size, [x[0] for x in self.results])
