from sklearn.preprocessing import normalize
import pandas as pd

class Datasource:
    '''
    General format of a datasource
    Format read by all our models and objective functions
    Allows efficient management of the data : preprocessing, sampling, split, ...
    Contains 
        + A dataframe containing the samples
        + A list of features
        + A sring for the columns containing the status of the loan (predictor)
    '''

    def __init__(self, data = None, features = None, label = None):
        self.data = data
        self.features = features
        self.label = label

    def getNextDataChunk(self):
        pass

    def getData(self):
        return self.data

    def getFeatures(self):
        return self.features

    def getLabel(self):
        return self.label

    def subsample(self, features):
        '''
        Allows to select only a subset of the features
        '''
        ds_subsample = Datasource(data = self.data, features=self.features, label=self.label)
        li_columns = [x for x in features]
        li_columns.append(self.getLabel())
        ds_subsample.data = ds_subsample.data[li_columns]
        ds_subsample.features = features
        return ds_subsample

    def balance_data(self, weight=0.5):
        '''
        Resample the data to force a balance in the data
        '''
        df_positive = self.data[self.data[self.label] == 1]
        df_negative = self.data[self.data[self.label] == 0]

        N_pos = int(weight * len(self.data[self.label]))
        N_neg = int((1 - weight) * len(self.data[self.label]))
        df_positive = df_positive.sample(N_pos, axis = 0, replace=True)
        df_negative = df_negative.sample(N_neg, axis = 0, replace=True)

        ds_balanced = Datasource(data=df_positive.append(df_negative), features = self.features, label=self.label)
        ds_balanced.data = ds_balanced.data.sort_index().reset_index(drop=True)

        return ds_balanced

    def normalize_data(self, sample_wise=False, norm = 'l2'):
        '''
        Noralize te data feature per feature or sample sample sample
        '''
        df_features = self.data[self.features]

        arr_normalized = normalize(df_features.values, norm=norm, axis=sample_wise)
        df_normalized = pd.DataFrame(index = df_features.index, columns=df_features.columns, data = arr_normalized)
        df_normalized[self.label] = self.data[self.label]

        self.data = df_normalized

    def train_dev_split(self, ratio=0.8):
        '''
        Creates a train and dev set from the datasource
        '''
        data = self.data.sample(frac=1).reset_index(drop=True)
        ds_train = Datasource(data = data.iloc[:int(ratio *len(data.index))],features = self.features,label = self.label)
        ds_test = Datasource(data = data.iloc[int(ratio *len(data.index)):],features = self.features,label = self.label)

        return ds_train, ds_test

    def cross_val_folds(self, N):
        '''
        Creates a list of tuples (datasource train, datasource test) for crossvalidation
        '''
        data = self.data.sample(frac=1).reset_index(drop=True)
        size = int(len(data[self.getLabel()]) / N)
        list_devtrain = []
        for i in range(N):
            index_test = [x for x in range(i*size, (i+1)*size)]
            index_train = [x for x in range(i*size)] + [x for x in range((i+1)*size, N *size)]
            ds_train = Datasource(data = data.iloc[index_train, :],features = self.features,label = self.label)
            ds_test = Datasource(data = data.iloc[index_test, :],features = self.features,label = self.label)

            list_devtrain.append((ds_train, ds_test))

        return list_devtrain