import pandas as pd
import numpy as np
import os
from .. import here
from .datasource import Datasource

class ChunkDS(Datasource):
    '''
    Subclass of Datasource
    Allows to load data from the csv containing features and samples directly
    '''

    def __init__(self, raw=False, here = here, samples=-1, skip = 0):

        if raw:
            directory = "raw_data"
            filename = "accepted_2007_to_2017.csv"
        else:
            directory = "First_Cleanup"
            filename = "cleaned_data.csv"

        if samples < 0:
            df = pd.read_csv(os.path.join(here, directory, filename), header=0, index_col=0, skiprows=list(range(1, skip+1)))
        else:
            df = pd.read_csv(os.path.join(here, directory, filename), header=0, nrows=int(samples), index_col=0, skiprows=list(range(1, skip+1)))

        df = df.dropna(axis = 0, how='any')
        self.data = df
        self.label = 'status'
        self.features = np.setdiff1d(self.data.columns.values, [self.label])

        self._raw = raw
        self._here = here
        self._samples = samples
        self._skip = skip

    def dump(self, filename, intermediate_folder=None):
        '''
        Dump results in a folder
        '''

        if not os.path.exists(os.path.join(here, "dump")):
            os.mkdir("dump")
        if intermediate_folder:
            if not os.path.exists(os.path.join(here, "dump", intermediate_folder)):
                os.mkdir(os.path.join(here, "dump", intermediate_folder))
            if os.path.exists(os.path.join(here, "dump", intermediate_folder, filename+".csv")):
                print("Error : the filename already exists in location")
                if input("Do you want to overwrite ? y/n") == 'y':
                    pass
                else:
                    return
            self.data.to_csv(os.path.join(here, "dump", intermediate_folder, filename+".csv"))
            print("saved in {}".format(os.path.join(here, "dump", intermediate_folder, filename+".csv")))

        else:
            if os.path.exists(os.path.join(here, "dump", filename+".csv")):
                print("Error : the filename already exists in location")
                if input("Do you want to overwrite ? y/n") == 'y':
                    pass
                else:
                    return
            self.data.to_csv(os.path.join(here, "dump", filename+".csv"))
            print("saved in {}".format(os.path.join(here, "dump", filename+".csv")))
