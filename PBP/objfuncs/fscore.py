from .objfunc import ObjFunc
import numpy as np
import pandas as pd


class Fscore(ObjFunc):
    def __init__(self, c = None):
        '''
        Takes Fscore as accuracy metric
        '''
        pass

    def evaluate(self, prediction, target):
        '''
        Compute the Fscore based on the accuracy matrix
        '''
        matrix = self.accuracy_matrix(prediction, target)

        precision = matrix.loc[1, 1] / (matrix.loc[0, 1] + matrix.loc[1, 1])
        recall = matrix.loc[1, 1] / (matrix.loc[1, 0] + matrix.loc[1, 1])
        if (precision == 0) or (recall == 0):
            result = 0
        else:
            result = 2 / (1 / precision + 1 / recall)
        return result

    def accuracy_matrix(self, prediction, target):
        '''
        Compute the accuracy matrix
        '''
        matrix = pd.DataFrame(index = [0, 1], columns = [0, 1])
        matrix.index.name = 'Reality'
        matrix.columns.name = 'Prediction'

        real_True = target == 1
        matrix.loc[0, 0] = (1 - prediction[np.logical_not(real_True)]).sum()
        matrix.loc[1, 0] = (1 - prediction[real_True]).sum()
        matrix.loc[0, 1] = prediction[np.logical_not(real_True)].sum()
        matrix.loc[1, 1] = prediction[real_True].sum()

        return matrix

