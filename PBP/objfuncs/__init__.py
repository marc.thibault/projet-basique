'''
Objfunction stores the metrics for evaluating the outputs of our models
'''

from .accuracy import Accuracy
from .fscore import Fscore
