from .objfunc import ObjFunc
import numpy as np
import pandas as pd


class Accuracy(ObjFunc):
    def __init__(self, c = None):
        '''
        Takes accuracy as evaluation metric
        '''
        if c is None:
            self.length = 0
            self.sum = 0
        else:
            self.length = c.length
            self.sum = c.sum

    def evaluate(self, prediction, target):
        '''
        Compares prediction and target, and returns the accuracy
        Sums the number of well predicted, and divides by the number of predictions
        '''
        self.length = len(prediction)
        self.sum = np.sum(prediction==target)

        return self.result()

    def accuracy_matrix(self, model, ds):
        '''
        Computes the accuracy matrix for a model and a datasource
        '''
        labels = ds.getData().loc[:, ds.getLabel()]
        preds = model.predict(ds)

        matrix = pd.DataFrame(index = [0, 1], columns = [0, 1])
        matrix.index.name = 'Reality'
        matrix.columns.name = 'Prediction'

        real_True = labels == 1
        matrix.loc[0, 0] = (1 - preds[np.logical_not(real_True)]).sum()
        matrix.loc[1, 0] = (1 - preds[real_True]).sum()
        matrix.loc[0, 1] = preds[np.logical_not(real_True)].sum()
        matrix.loc[1, 1] = preds[real_True].sum()

        return matrix

    def result(self):
        return self.sum/self.length

    def add(self, other):
        '''
        Used to concatenate two predictions
        '''
        a = Accuracy(other)
        a.length += self.length
        a.sum += self.sum

        return a
