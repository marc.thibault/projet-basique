---
output:
  pdf_document: default
  html_document: default
---
# Project proposal: Dynamic loan default prediction

Category: Finance & Commerce

Maxime Rivet, SUNetID: 06228390

Marc Thibault, SUNetID: 06227968

Mael Trean, SUNetID: 06228438




## Motivation

Predicting loan default is a crucial step for risk management in insurance companies.

As a way to assess their risk, credit companies have to estimate their debtors' solvavility. They need to find reliable ways of predicting whether a contract will lead to default or not - and evaluate the quality of the loan consequently.

Our objective in this project will be in the first place to find a correct way to determine if a given loan will lead to a default given parameters describing the client's initial and current information.

As a second objective, we will be looking at the state of solvability and the probability of default of current loans, given the history of payments, as a dynamic estimation.

## Data

Our dataset comes from the Kaggle `https://www.kaggle.com/wendykan/lending-club-loan-data`. This data consists in all the applications received by Lending Club, with relevant information about the applicant, the decision of Lending Club and the outcome of the loan (default or not).

We have 75 features for each sample (about $500,000$ loans), such as age, gender, localization, credit score, revenue, etc.


## Methods

#### Dataset Exploration

The first step will be to visualize the data by plotting individual features distributions, and their relation to the target function, that is the loan status.

We will use correlation analysis to reduce the number of features, in order to remove redundant ones. A PCA will be relevant for this task.

A crucial part will be feature selection, in order to select the most explanative features for the problem.

#### Intended experiments

Our predicator will output a decision for each loan application of the test set. We will first try to evaluate the amount of loans which are deemed safe by our algorithm but which will lead to a default. Those are the most dangerous for the loaner, ergo we should control these first. The metric to minimize would be the Recall ratio:

$$Recall = \frac{\#defaulted\ loans\ considered\ as\ risky}{\#defaulted\ loans}$$

The other crucial metric to consider is the True Positive ratio:

$$Precision = \frac{\#defaulted\ loans\ considered\ as\ risky}{\#loans\ considered\ as\ risky}$$

We will be using these two metrics with $F$-measure and ROC curve analysis.




#### Basic Methods

We will use the following basic methods in the first place:

- Logistic regression,

- Support Vector Machine,

- Decision Tree,

- Gradient Boosting Trees.

We will use Python 3.6 for this project, with the libraries:

- `scikit-learn`

- `xgboost`

Depending on the result of these initial experiments, we will refine these algorithms, as explained below.


#### Enhancements

If our pace of progress is sufficient and we are not satisfied with the previous results, we will go for the following approach:

We will try to use a temporal approach, using the history of previous payments for credit card/loan data, in order to predict an upcoming default based on updated information on the customer. Our Machine Learning tools will at this point be Hidden Markov Models, and Recurrent Neural Networks.
