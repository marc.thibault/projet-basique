---
output:
  pdf_document: default
  html_document: default
---
# Project Milestone: Dynamic loan default prediction

This first part of the project allowed us to clean the dataset and setup a clean pipeline in order to test various model easily and quickly. After these first important steps, we have been able to confront our first models to the data and to realize that our real challenge will be to deal with the imbalanced representation of the classes.  

##Project architecture

We have chosen to organize our machine learning pipeline in a highly structured way, in order make the standardization and the scaling up of our setup easier.

<center>

<img src="architecture.png" height="300px" />**Architecture of the main classes of the project**</center>

#### DataSource

The data input is entirely handled by the `DataSource` class and its children. A `DataSource` object can be built through one of its implementations, using a data file as an input. Eventually, it is made to be handled via the following standard interface:

- `getData()` returns the underlying data of the `DataSource` as a `pandas` table;

- `getLabel()` returns the name of the target feature in the `DataSource` pandas;

- the `getFeatures()` function returns the name of the feature columns we can use as a features in further machine learning algorithms, in the `DataSource` pandas.

We have implemented the `ChunkDS` class inheriting from `DataSource`, implementing the access to the data through `.csv` files. Furthermore, it provides data chunk by chunk, and allows skipping any number of lines in the input `.csv` file, without having any memory impact.

The `FeaturesSelection`class inherits from `ChunkDS`. It is not performing a machine learning selection. It narrows the features of a `ChunkDS` object to the only features we are allowed to use in the Lending Club dataset. These indicators are those which are available upon signature of a loan. Doing so, we remove the information which was gathered after the loan initiation.

#### Model

We have created the `Model` class, which can be interacted with through an interface close to that of `sklearn`. The difference is that we implemented so that it interacts with `DataSource` objects. A `Model` implementation has the following methods:

- the `fit(ds)` function takes a `DataSource` as an input. It returns nothing, but performs the fitting of the model on a training dataset. The optimized parameters are keep stored in the `Model` object;

- the `predict(ds)` function also takes a `DataSource` as an input. It only uses the feature columns, and outputs the model's prediction based on what it has learned. The classification prediction is a DataFrame with 1s and 0s;

- the `predict_proba(ds)` is the same as the previous one, the only difference being that it outputs a float number between 0 and 1 for each sample, representing the probability that the models guesses for the sample of being classified as positive.

`SKlearnWrapper` is a child class of `Model`. It takes a `BaseEstimator` object, following the `sklearn` standards, and it does the interfacing with the `DataSource` objects.


#### FrontierSelection

`FrontierSelection` is a child class of `Model`. We noticed a huge unbalance between the categories we are trying to predict (15% of positive samples, 85% of negative samples). Consequently, when using a vanilla classification engine, we very often end up always predicting "negative".

This wrapper takes a model as an input. Upon a `fit` call, it splits the provided data into a train and a dev set. The model is trained on the train set. The model outputs float results $p_i$ between 0 and 1 when classifying the dev set. Then we iterate over float number $\alpha$ between 0 and 1, producing the classifier:

$$
f(i) = 1_{{p_i > \alpha}}
$$
This allows choosing where the separating frontier holds, and precisely tuning Precison and Recall. We then choose the separating $\alpha$ which optimize the objective function we want, such as the F-score.

## Feature selection

The lending club dataset is very large, with more than 1.5 million of loans registered and 129 parameters concerning each. An important part of our work so far consisted in cleaning the dataset to be fed into `ChunkDS`.

#### Data cleaning

The data was very large, but several features were irrelevant to our study:  
+ More than half of the loans are considered as active in the dataset: we don't have the outcome, and thus we cannot use them to predict
+ 51 parameters consist into data that was gathered after the signature of the loan : we cannot use them as predictors
+ 47 parameters are extremely sparse (more than 60% empty): we decided to get rid of them
+ 99% of the population has the same value for parameters in 11 features: for robustness, we dropped them.
+ We transformed data of type `Number of past bankruptcies` to categorical features `Past bankruptcies?`.

#### Data overview

After this cleaning procedures, we had the following dataset:
+ 750,000 samples of completed loans
+ 19 relevant features for each sample

The data is very highly unbalanced, since 75% of the loans are fully paid: this aspect will lead us to implement corrections in our models.

## First results

We have first run a very basic Logistic Regression over the dataset. In order to evaluate the performance of our algorithm, we have implemented two validation metrics:
+ Precision, which is the percentage of well classified predictions
+ F score, which is the harmonic average between recall and precision
  + $recall = \frac{\#\text{(classified as 1 | reality is 1)}}{\#\text{(reality is 1)}}$
  + $precision = \frac{\#\text{(classified as 1 | reality is 1)}}{\#\text{(classified as 1)}}$

Those second metric is more relevant for our task since we are more interested in rejecting the loans that will default than in accepting loans that are safe.

This metric will be used to validate the performance of our algorithm.

## Logistic regression

#### Unbalanced data

We ran first a basic logistic regression on the data. We took 500,000 samples as training set, and 100,000 as test set.

The performance seems at first sight to be very good, with an accuracy of **74%**. 
However, this is achieved by classifying most of the samples from the test set as being safe, which achieves a good precision due to the unbalance in the dataset. The f-score in this case is only **0.037%**, which is very low.

The results are given here :
| Prediction |    0    |  1   |
| ---------: | :-----: | :--: |
|   **True** |         |      |
|          0 | 128,387 |  17  |
|          1 | 44,131  |  8   |

#### Balanced data

The first natural thing that we tested to solve this problem was to force the training set to be balanced, in order to make the algorithm see for features linked to default, and thus make it more able to detect it and to predict correctly.

This approach was quite successful : even is the overall accuracy dropped to **61.2%**, we managed to get a F-score of **46.4%**. The algoritm predicted spotted more than **63%** of the defaulting loans, which is better, but it also only rejected **20%** of the loans appropriately, leading to a very bad precison.

The results are given here :
| Prediction |   0    |   1    |
| ---------: | :----: | :----: |
|   **True** |        |        |
|          0 | 76,797 | 51,607 |
|          1 | 15,193 | 28,946 |

## Frontier selection

Balancing the dataset is improving the results, but it does not make them good enough to be satisfactory. To improve the results, we decided to optimize the frontier between positive and negative samples by crossvalidation through the `FrontierSelection` module.

This module aims to classify as defaulted samples for which the algorithm is not very confident that they are safe: instead of classifying as defaulted the samples for which the algorithm outputs 0.5 and more, we can select a boundary that is smaller, so we detect samples that are not very safe according to the algoritm.

#### Logistic regression

We first implemented this algorithm on a Logistic Regression. Without balancing the data, we managed to reach a comparable F-score for the Logistic Regression, but with a lower accuracy : we get **40.5%** for the F-score, but with only **54%** in accuracy.

The frontier that is selected is 0.19: as soon as the Logistic Regressor yields an output larger than this value, we predict that the loan will default. This mechanically leads to a better F-score, but lowers the precision. Below is represented the F-score compared to the frontier for this setup, for the dev set only:

<center>

<img src="../images/F-score_Logistic.png" height="200px" />

</center>

We see that the F-score drops dramatically when the frontier increases. This solution can be an alternative to balancing the data. Let us try to run both techniques at the same time.

We ran the same algorithm but his time with balanced data. The results are a bit worse than what was obtained through a classical Logistic regression: the F-test and the precision is lower, which makes this approach not accurate in complement to a balancing of the data.

Moreover, this method depends highly on the dev set that is picked, and the results change a lot from one run to the other.

#### Gradient Boosting

We finally tried to use Gradient Boosting classification algorithm to predict the loan status. We did the same reasoning as for the logistic regression : unbalanced, balanced, frontier selection and both.

###### Unbalanced

The results for the unbalanced model is slightly better than the unbalanced Logistic Regression: we manage to get a F-score of **8%** (compared to **0.03%**) with the same precision of around **75%**

###### Balanced

The results for the balanced way is also slightly better that the Logistic Regression, with a F-score of **49%** (compared to **46%**) and a precision of **64%** (compared to **61%**)

###### Frontier Selection

The Frontier Selection process for the Gradient Boosting is also very good, reaching an accuracy of **67%** (better than the **51%** of the logistic Regression) and a F-score of **48.7%** (compared to **40%**). The score curve is plotted below:

<center>

<img src="../images/F-score_Boosting.png" height="200px" />

</center>

###### Cumulative effect

The effect of applying the frontier selection on balanced data is however not very satisfactory, since the results are lower than the same as in the balanced case.

In general, the Gradient Boosting algorithm performs better than the logistic Regression, and reaches quite good results through our frontier selection process. However, the frontier selection process seems to be a bit unstable for now, and we will have to improve it to make it more robust and more likely to yield satisfactory results.



## Next Steps



While testing these firsts solutions, we obtained a clearer view on this challenge. Imbalanced classes representation force us to adapt our cross validation scheme as well as the way we treat the output of our classification algorithms, and to find new way to assess the quality of our classdification algorithms. These are exactly the difficulties we will continue to work on. Moreover, our feature engineering so far has been really simple, it might be another lead to follow. Some ideas might be to use Synthetic Minority Over-Sampling Techniques, generating new samples of our underrepresented class. We will also continue to search in the direction of ensembling techniques as boosting. 

