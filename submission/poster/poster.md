# Dynamic loan default prediction

  **Members** :
Mael Trean      mtrean@stanford.edu
Marc Thibault   marcthib@stanford.edu
Maxime Rivet    mrivet@stanford.edu

## Motivation

Predicting the ouctome of a loan is a recurrent, crucial and difficult issue in insurance and banking.   
The objective of our project is to predict whether a loan will default or not based on objective financial data only.  
We used a dataset provided by LendingClub concerning almost 1 million loans issued between 2008 and 2017.  
Since a default is far more costly for a loan issuer than a missed loan, we focused on maximizing the F-score as an evaluation metric for our algorithm.  
Using a very structured pipeline to load and test algorithm, we reviewed most of the classification Machine Learning strategies to extract information from the very noisy data provided by LendingClub.

## Data

The data is taken from a kaggle project provided by LendingClub. It consists into approximately 800,000 samples of loans granted by the company, with the full set of informations about the borrower, the history of payments and the outcome of the loan. The dataset is quite clean and the figures can be considered as ground truth, but lots of columns are either irrelevant, very sparse or non informative. Moreover, the dataset is very unbalanced, with approximately 17% of loans considered as defaulted.

## Features

Since the objective is to predict the outcome from the informations gathered at the signature of the loan, we cannot use the data concerning the history of payments or the current situation of a loan. Excluding features for which the information is icomplete, or uninformative, we get a total of 19 features, that cover personal information (credit grade, income, housing status, ...) and credit information (amount, interest rates, ...).

## Models used

We fitted mainly two models, with custom modifications : Logistic REgression and Gradient Boosting. Since it is impossible to use the F score directly as a loss function, we used three methods to improve the F-score through cross-validation:
+ Resample the training set to change the proportion of defaulted loans
+ Change the limit above which the algorithm outputs 1 from the continuous output of the prediction : $prediction = (output > limit) * 1$
+ For the Logistic Regression only, change the loss function to put more weight on prediction correctly the outputs labeled as defaulted : $loss = \alpha y log(\hat{y}) + \beta (1 - y) log(1 - \hat{y})$

## Results and discussion

Regarding the balance between defaulted and fully paid loans in the dataset, predicting the output randomly yields a F-score comprised between 0 and 30%, and we have to make sure to beat this limit. The dataset is clearly not well separated, as this PCA denotes : ![Image of the PCA](PCA)

Our algorithm consistently beat the random choice. However, due to the noisiness of the data, we cannot expect to have a very large F-score, and our three approaches floor at approximately 40% of F-score.

![Resample](resample)
![FrontierSelction](Frontier)
![Weight change](Weight)

We do not manage to achieve a very good classification through those methods.

Basic Neural Networks and basic SVMs do not manage better as well.

We tried a non parametric approach, as follows :
+ run a dimension redction on the data simultaneously with train and test(PCA or TSNE)
+ Fit on the train part and predict an output for the test part

This method does not yield a very good result either.

## What next

Shit on the streets
Poop on the floor

