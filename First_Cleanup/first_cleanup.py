import pandas as pd
import numpy as np
import os

df_data = pd.read_csv(os.path.join('raw_data', 'accepted_2007_to_2017.csv'), header=0)
df_desc = pd.read_csv('data_dict.csv', header=0, index_col = 0)
df_desc = df_desc.set_index('name')

li_drop = []

for col in df_data.columns:
    if df_data[col].dtype == object:
        df_data[col] = df_data[col].fillna("NaN")
        li_elts = sorted(list(set(df_data[col].values)))
        if col == "sub_grade":
            df_data[col] = df_data[col].apply(lambda x : list(x)[-1])
            df_data[col] = df_data[col].replace("N", np.nan)
            df_desc.loc[col, "comment"] = li_elts[0] + " changed to" + list(li_elts[0])[-1]
        elif col == "emp_length":
            df_data[col] = df_data[col].apply(lambda x : x.split()[0])
            df_data[col] = df_data[col].replace("NaN", np.nan)
            df_data[col] = df_data[col].replace("10+", 10)
            df_desc.loc[col, "comment"] = li_elts[0] + " changed to" + li_elts[0].split()[0]
        elif col == "zip_code":
            df_data[col] = df_data[col].apply(lambda x : ''.join(list(x)[:3]))
            df_data[col] = df_data[col].replace("NaN", np.nan)
            df_desc.loc[col, "comment"] = li_elts[0] + " changed to" + ''.join(list(li_elts[0])[:3])
        elif col.split("_")[-1] == 'd':
            df_data[col] = df_data[col].apply(lambda x : x.split("-")[-1])
            df_data[col] = df_data[col].replace("NaN", np.nan)
            df_desc.loc[col, "comment"] = li_elts[0] + " changed to" + li_elts[0].split("-")[-1]
        else:
            li_elts = [x for x in li_elts if x != "NaN"]
            N = len(li_elts)
            df_data[col] = df_data[col].replace("NaN", np.nan)
            dic_conv = {}
            if N < 15:
                for i in range(N):
                    dic_conv[li_elts[i]] = i
                df_data[col] = df_data[col].replace(dic_conv)
                df_desc.loc[col, "comment"] = str(dic_conv)
            else:
                li_drop.append(col)
                df_desc.loc[col, "comment"] = "dropped"
    
    elif df_data[col].dtype == float:
        if np.isnan(df_data[col].max()):
            df_desc[col, 'comment'] = 'column is empty'

df_data = df_data.drop(li_drop, axis=1)
df_desc.to_csv('data_dict_final.csv')
df_data.to_csv('mapping_cleaned.csv')
df_data.iloc[:10].to_csv('head_mapping.csv')